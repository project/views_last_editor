Views Last Editor is a Drupal module that makes the user who edited a node
available to the Views module; much in the same way as the author of a node
is available to the Views module.

Author: Oswald Jaskolla <oswald@jaskolla.net> 